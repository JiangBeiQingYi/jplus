/**
 * Copyright (c) 2017-2018,Retire 吴益峰 (372310383@qq.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.jplus;

import com.google.inject.Binder;
import com.jfinal.config.Constants;
import com.jfinal.config.Interceptors;
import com.jfinal.ext.handler.ContextPathHandler;
import com.jfinal.ext.interceptor.SessionInViewInterceptor;
import com.jfinal.template.Engine;
import io.jboot.Jboot;
import io.jboot.aop.jfinal.JfinalHandlers;
import io.jboot.aop.jfinal.JfinalPlugins;
import io.jboot.server.ContextListeners;
import io.jboot.server.Servlets;
import io.jboot.server.listener.JbootAppListenerBase;
import io.jplus.core.handler.RenderTimeHandler;


public class Starter extends JbootAppListenerBase {

    @Override
    public void onJfinalEngineConfig(Engine engine) {
        //设置为开发模式
        engine.setDevMode(Jboot.me().isDevMode());
    }

    @Override
    public void onJfinalConstantConfig(Constants constants) {
        constants.setError404View(Consts.BASE_VIEW_PATH + "common/404.html");
        constants.setError500View(Consts.BASE_VIEW_PATH + "common/500.html");
        constants.setErrorView(401, Consts.BASE_VIEW_PATH + "common/401.html");
        constants.setErrorView(403, Consts.BASE_VIEW_PATH + "common/403.html");
    }


    @Override
    public void onInterceptorConfig(Interceptors interceptors) {
        super.onInterceptorConfig(interceptors);
        interceptors.add(new SessionInViewInterceptor());
    }

    @Override
    public void onHandlerConfig(JfinalHandlers handlers) {
        super.onHandlerConfig(handlers);
        handlers.add(new RenderTimeHandler());
        handlers.add(new ContextPathHandler("CPATH"));
    }

    @Override
    public void onJfinalPluginConfig(JfinalPlugins plugins) {
        super.onJfinalPluginConfig(plugins);
    }

    @Override
    public void onJbootDeploy(Servlets servlets, ContextListeners listeners) {
        super.onJbootDeploy(servlets, listeners);
    }


    @Override
    public void onGuiceConfigure(Binder binder) {
        super.onGuiceConfigure(binder);
        //binder.bindInterceptor(Matchers.any(), Matchers.annotatedWith(LogConfig.class), new JplusLogInterceptor());
    }

    @Override
    public void onJbootStarted() {

    }

    public static void main(String[] args) {
        Jboot.run(args);
    }
}
