/**
 * Copyright (c) 2017-2018,Retire 吴益峰 (372310383@qq.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.jplus.wechat.controller;

import com.jfinal.weixin.sdk.api.ApiResult;
import io.jboot.web.controller.annotation.RequestMapping;
import io.jboot.wechat.controller.JbootWechatController;
import io.jplus.Consts;
import org.apache.shiro.authz.annotation.RequiresUser;

@RequestMapping(value = "/wechat", viewPath = Consts.BASE_VIEW_PATH+"/")
public class IndexController extends JbootWechatController {

    @RequiresUser
    public void main(){
        render("main.html");
    }

    public void login(){
        render("index.html");
    }

    @Override
    public Object doGetUserByOpenId(String openid) {
        return null;
    }

    @Override
    public Object doSaveOrUpdateUserByApiResult(ApiResult apiResult) {
        return null;
    }
}

