/**
 * Copyright (c) 2017-2018,Retire 吴益峰 (372310383@qq.com).
 *
 * Licensed under the GNU Lesser General Public License (LGPL) ,Version 3.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.jplus.core.base;

import com.jfinal.plugin.activerecord.Page;
import io.jboot.db.model.Column;
import io.jboot.db.model.JbootModel;

import java.util.List;

public interface BaseService<M extends JbootModel<M>> {

    default public Page<M> paginateByColumns(int page, int size, List<Column> columns, String orderBy){
        return null;
    }

}
