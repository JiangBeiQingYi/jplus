/**
 * Copyright (c) 2017-2018,Retire 吴益峰 (372310383@qq.com).
 *
 * Licensed under the GNU Lesser General Public License (LGPL) ,Version 3.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.jplus.core.directive;

import com.jfinal.template.Env;
import com.jfinal.template.io.Writer;
import com.jfinal.template.stat.Scope;
import io.jboot.web.directive.annotation.JFinalDirective;
import io.jboot.web.directive.base.JbootDirectiveBase;


@JFinalDirective("JplusButton")
public class JplusButtonDirective extends JbootDirectiveBase {
    @Override
    public void onRender(Env env, Scope scope, Writer writer) {
        String id = getParam("id", "", scope);
        String name = getParam("name", scope);
        String icon = getParam("icon", scope);
        String btnType = getParam("btnType", "primary", scope);
        String onclick = getParam("onclick", scope);
        boolean spaceCss = getParam("spaceCss", true, scope);
        String spaceStr = "";
        if (spaceCss) {
            spaceStr = " button-margin";
        }

        String html = "<button type=\"button\" class=\"btn btn-" + btnType + spaceStr + "\" onclick=\"" + onclick + "\" id=\"" + id + "\"> <i class=\"fa " + icon + "\"></i>&nbsp;" + name + " </button>";
        write(writer, html);
    }
}
