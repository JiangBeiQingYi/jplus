/**
 * Copyright (c) 2017-2018,Retire 吴益峰 (372310383@qq.com).
 *
 * Licensed under the GNU Lesser General Public License (LGPL) ,Version 3.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.jplus.core.directive;

import com.jfinal.template.Env;
import com.jfinal.template.io.Writer;
import com.jfinal.template.stat.Scope;
import io.jboot.web.directive.annotation.JFinalDirective;
import io.jboot.web.directive.base.JbootDirectiveBase;


@JFinalDirective("JplusTable")
public class JplusTableDirective extends JbootDirectiveBase {
    @Override
    public void onRender(Env env, Scope scope, Writer writer) {
        String id = getParam("id", "dataTable", scope);
        String html = "<table id=\"" + id + "\" data-mobile-responsive=\"true\" data-click-to-select=\"true\"> <thead> <tr>  <th data-field=\"selectItem\" data-checkbox=\"true\"></th> </tr> </thead> </table>";
        write(writer, html);
    }
}
