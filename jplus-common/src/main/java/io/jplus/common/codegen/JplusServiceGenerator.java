/**
 * Copyright (c) 2017-2018,Retire 吴益峰 (372310383@qq.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.jplus.common.codegen;

import com.jfinal.plugin.activerecord.generator.MetaBuilder;
import com.jfinal.plugin.activerecord.generator.TableMeta;
import io.jboot.codegen.CodeGenHelpler;
import io.jboot.codegen.service.JbootServiceGenerator;
import io.jboot.codegen.service.JbootServiceImplGenerator;
import io.jboot.codegen.service.JbootServiceInterfaceGenerator;
import org.apache.commons.lang.StringUtils;

import java.util.List;

/**
 *
 */
public class JplusServiceGenerator extends JbootServiceGenerator {

    public JplusServiceGenerator(String basePackage, String modelPackage) {
        super(basePackage, modelPackage);
    }


    public static void run(String basePackage, String modelPackage, String prefix) {
        doGenerateWith(basePackage, modelPackage, null, null, prefix);
    }

    /**
     * 只生成某些表
     */
    public static void runWith(String basePackage, String modelPackage, String includeTables) {
        doGenerateWith(basePackage, modelPackage, includeTables);
    }

    /**
     * 只生成某些表
     */
    public static void runWith(String basePackage, String modelPackage, String includeTables, String excludeTables) {
        doGenerateWith(basePackage, modelPackage, includeTables, excludeTables, null);
    }

    public static void doGenerateWith(String basePackage, String modelPackage, String includeTables) {

        doGenerateWith(basePackage, modelPackage, includeTables, null, null);

    }


    public static void doGenerateWith(String basePackage, String modelPackage, String includeTables, String excludeTables, String prefix) {

        System.out.println("start generate...");
        MetaBuilder metaBuilder = CodeGenHelpler.createMetaBuilder();
        metaBuilder.addExcludedTable(StringUtils.split(excludeTables, ","));
        metaBuilder.setRemovedTableNamePrefixes(StringUtils.split(prefix, ","));
        List<TableMeta> tableMetaList = metaBuilder.build();
        JplusModelGenerator.withIncludeTables(tableMetaList, includeTables);

        new JbootServiceInterfaceGenerator(basePackage, modelPackage).generate(tableMetaList);
        new JbootServiceImplGenerator(basePackage, modelPackage).generate(tableMetaList);

        System.out.println("service generate finished !!!");

    }
}
