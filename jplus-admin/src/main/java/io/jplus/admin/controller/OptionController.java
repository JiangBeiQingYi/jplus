/**
 * Copyright (c) 2017-2018,Retire 吴益峰 (372310383@qq.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.jplus.admin.controller;

import io.jboot.core.rpc.annotation.JbootrpcService;
import io.jboot.web.controller.annotation.RequestMapping;
import io.jplus.Consts;
import io.jplus.admin.model.Option;
import io.jplus.admin.service.OptionService;
import io.jplus.core.base.BaseController;

import java.util.List;

@RequestMapping(value = "/admin/option",viewPath = Consts.BASE_VIEW_PATH+"admin/option")
public class OptionController extends BaseController{

    @JbootrpcService
    OptionService optionService;

    @Override
    public void index() {
        List<Option> list = optionService.findAll();
        setAttr("list",list);
        render("edit.html");
    }

    public void save(){

    }

}
