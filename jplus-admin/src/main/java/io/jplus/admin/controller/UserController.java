/**
 * Copyright (c) 2017-2018,Retire 吴益峰 (372310383@qq.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.jplus.admin.controller;

import com.jfinal.aop.Before;
import com.jfinal.core.paragetter.Para;
import com.jfinal.ext.interceptor.GET;
import com.jfinal.ext.interceptor.POST;
import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.Page;
import io.jboot.core.rpc.annotation.JbootrpcService;
import io.jboot.db.model.Columns;
import io.jboot.web.controller.annotation.RequestMapping;
import io.jboot.web.render.JbootRender;
import io.jplus.Consts;
import io.jplus.admin.model.Dept;
import io.jplus.admin.model.User;
import io.jplus.admin.service.DeptService;
import io.jplus.admin.service.RoleService;
import io.jplus.admin.service.UserService;
import io.jplus.core.base.BaseController;
import io.jplus.utils.EncryptUtils;

@RequestMapping(value = "/admin/user", viewPath = Consts.BASE_VIEW_PATH + "admin/user")
public class UserController extends BaseController {

    @JbootrpcService
    UserService userService;
    @JbootrpcService
    RoleService roleService;
    @JbootrpcService
    DeptService deptService;


    public void list() {
        Columns columns = Columns.create();
        String uname = getPara("uname");
        if (StrKit.notBlank(uname)) {
            columns.like("name", uname);
        }

        String deptId = getPara("deptid");
        if (StrKit.notBlank(deptId)) {
            columns.eq("deptid", deptId);
        }

        String beginTime = getPara("beginTime");
        if (StrKit.notBlank(beginTime)) {
            columns.gt("createtime", beginTime);
        }

        String endTime = getPara("endTime");
        if (StrKit.notBlank(endTime)) {
            columns.lt("createtime", endTime);
        }

        String orderBy = getPara("orderBy", "id");

        Page<User> page = userService.paginateByColumns(getPageNumber(), getPageSize(), columns.getList(), orderBy);
        renderPageForBT(page);
    }

    @Before(GET.class)
    public void edit(String userId) {
        User user = new User();
        if (StrKit.notBlank(userId)) {
            user = userService.findById(userId);
            Dept dept = deptService.findById(user.getDeptid());
            user.put("deptName", dept.getFullname());
        }
        setAttr("user", user);
        render("edit.html");
    }

    @Before(POST.class)
    public void save(@Para("user") User user) {
        User tmpUser = userService.findByAccount(user.getAccount());
        if(tmpUser!=null){
           renderAjaxResultForError("用户名已存在！");
           return;
        }
        boolean tag = userService.save(user);
        renderJson(tag);
    }

    public void delete(Integer userId) {
        boolean tag = userService.deleteById(userId);
        if (tag) {
            renderAjaxResultForSuccess();
        } else {
            renderAjaxResultForError("删除失败！");
        }
    }

    public void reset(Integer userId) {
        User user = userService.findById(userId);
        user.setPassword(EncryptUtils.encryptPassword("111111", user.getSalt()));
        boolean tag = userService.update(user);
        if (tag) {
            renderAjaxResultForSuccess();
        } else {
            renderAjaxResultForError("密码重置失败！");
        }
    }

    public void freeze(Integer userId) {
        User user = userService.findById(userId);
        user.setStatus(Consts.USER_STATUS_DISABLE);
        boolean tag = userService.update(user);
        if (tag) {
            renderAjaxResultForSuccess();
        } else {
            renderAjaxResultForError("用户冻结失败！");
        }
    }

    public void unfreeze(Integer userId) {
        User user = userService.findById(userId);
        user.setStatus(Consts.USER_STATUS_ENABLE);
        boolean tag = userService.update(user);
        if (tag) {
            renderAjaxResultForSuccess();
        } else {
            renderAjaxResultForError("用户解冻失败！");
        }
    }

    public void userInfo() {

        render(new JbootRender("userInfo.html"));
    }

    public void changePwd() {
        render(new JbootRender("changePwd.html"));
    }
}
