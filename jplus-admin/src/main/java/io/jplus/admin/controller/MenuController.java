/**
 * Copyright (c) 2017-2018,Retire 吴益峰 (372310383@qq.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.jplus.admin.controller;

import com.jfinal.aop.Before;
import com.jfinal.core.paragetter.Para;
import com.jfinal.ext.interceptor.POST;
import com.jfinal.kit.StrKit;
import io.jboot.core.rpc.annotation.JbootrpcService;
import io.jboot.db.model.Columns;
import io.jboot.web.controller.annotation.RequestMapping;
import io.jplus.Consts;
import io.jplus.admin.model.Menu;
import io.jplus.admin.service.MenuService;
import io.jplus.core.base.BaseController;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RequestMapping(value = "/admin/menu", viewPath = Consts.BASE_VIEW_PATH+"admin/menu")
public class MenuController extends BaseController {

    @JbootrpcService
    MenuService menuService;


    public void list() {
        String level = getPara("level");
        String menuName = getPara("menuName");
        Columns columns = Columns.create();
        if (StrKit.notBlank(level)) {
            columns.eq("level", level);
        }

        if (StrKit.notBlank(menuName)) {
            columns.like("name", menuName);
        }
        List<Menu> menuList = menuService.findByColumn(columns);
        renderJson(menuList);
    }

    public void edit(String menuId) {
        Menu menu;
        if (StrKit.isBlank(menuId)) {
            menu = new Menu();
        } else {
            menu = menuService.findById(menuId);
            Menu parent = menuService.findById(menu.getPid());
            if (parent != null) {
                menu.put("pidName", parent.getName());
            }
        }
        setAttr("menu", menu);
        render("edit.html");
    }

    public void delete(String menuId) {
        boolean tag = menuService.deleteById(menuId);
        if (tag) {
            renderAjaxResultForSuccess();
        } else {
            renderAjaxResultForError("");
        }
    }


    @Before(POST.class)
    public void save(@Para("")Menu menu) {
        boolean tag = menuService.save(menu);
        if (tag) {
            renderAjaxResultForSuccess();
        } else {
            renderAjaxResultForError("保存报错！");
        }
    }

    @Before(POST.class)
    public void update(@Para("")Menu menu) {
        boolean tag = menuService.update(menu);
        if (tag) {
            renderAjaxResultForSuccess();
        } else {
            renderAjaxResultForError("更新报错！");
        }
    }

    public void tree() {
        Columns columns = Columns.create();
        columns.eq("ismenu", 1);
        List<Menu> menuList = menuService.findByColumn(columns);
        renderJson(buildTree(menuList));
    }

    private List<Map<String, Object>> buildTree(List<Menu> menuList) {
        List<Map<String, Object>> tree = new ArrayList<>();
        for (Menu menu : menuList) {
            Map map = new HashMap(4);
            map.put("id", menu.getId());
            map.put("name", menu.getName());
            map.put("pid", menu.getPid());
            map.put("open", true);
            tree.add(map);
        }
        return tree;
    }
}
