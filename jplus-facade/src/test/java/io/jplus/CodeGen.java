/**
 * Copyright (c) 2017-2018,Retire 吴益峰 (372310383@qq.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.jplus;

import io.jplus.common.codegen.JplusModelGenerator;
import io.jplus.common.codegen.JplusServiceGenerator;

public class CodeGen {

    public static void main(String[] args) {
        String modelPackage = "io.jplus.admin.model";
        String basePackage = "io.jplus.admin.service";

        //指定生成的表
        //String[] tables = {"TEST"};
        //要排除的表，主要是没有主键的表
        //String[] excludeTables = {"TBL_A"};
        //JplusModelGenerator.runWith(modelPackage, StringUtils.join(tables, ","), StringUtils.join(excludeTables, ","));
        //JplusServiceGenerator.runWith(basePackage, modelPackage, StringUtils.join(tables, ","), StringUtils.join(excludeTables, ","));

        //默认生成全部
        JplusModelGenerator.run(modelPackage,"jp_");
        JplusServiceGenerator.run(basePackage, modelPackage,"jp_");
    }

}
