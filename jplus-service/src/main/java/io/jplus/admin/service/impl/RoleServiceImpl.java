/**
 * Copyright (c) 2017-2018,Retire 吴益峰 (372310383@qq.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.jplus.admin.service.impl;

import com.google.inject.Singleton;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.SqlPara;
import io.jboot.aop.annotation.Bean;
import io.jboot.core.rpc.annotation.JbootrpcService;
import io.jboot.db.model.Column;
import io.jboot.service.JbootServiceBase;
import io.jplus.admin.model.Role;
import io.jplus.admin.service.RoleService;

import java.util.List;

@Singleton
@Bean
@JbootrpcService
public class RoleServiceImpl extends JbootServiceBase<Role> implements RoleService {

    @Override
    public Page<Role> paginateByColumns(int page, int size, List<Column> columns, String orderBy) {
        Page<Role> pages = DAO.paginateByColumns(page, size, columns, orderBy);
        this.join(pages,"pid","pName");
        return pages;
    }

    @Override
    public List<Role> findByColumns(List<Column> columns) {
        List<Role> roles = DAO.findListByColumns(columns);
        this.join(roles,"pid","pName");
        return roles;
    }

    @Override
    public List<Role> findByColumn(Column column) {
        return DAO.findListByColumn(column);
    }

    @Override
    public List<Role> findByUserId(Integer userId) {
        SqlPara sqlPara = Db.getSqlPara("admin-role.findByUserId");
        sqlPara.addPara(userId);
        return DAO.find(sqlPara);
    }

}
