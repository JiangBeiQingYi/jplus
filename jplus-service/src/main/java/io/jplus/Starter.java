/**
 * Copyright (c) 2017-2018,Retire 吴益峰 (372310383@qq.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.jplus;

import com.google.inject.Binder;
import com.jfinal.plugin.druid.DruidStatViewHandler;
import com.jfinal.template.Engine;
import io.jboot.Jboot;
import io.jboot.aop.jfinal.JfinalHandlers;
import io.jboot.server.listener.JbootAppListenerBase;


public class Starter extends JbootAppListenerBase {

    @Override
    public void onJfinalEngineConfig(Engine engine) {
        //设置为开发模式
        engine.setDevMode(Jboot.me().isDevMode());
    }

    @Override
    public void onHandlerConfig(JfinalHandlers handlers) {
        super.onHandlerConfig(handlers);
        handlers.add(1,new DruidStatViewHandler("/admin/druid"));
    }

    @Override
    public void onGuiceConfigure(Binder binder) {
        super.onGuiceConfigure(binder);
    }

    public static void main(String[] args) {
        Jboot.run(args);
    }
}
