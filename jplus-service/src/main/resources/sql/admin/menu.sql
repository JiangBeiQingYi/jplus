#sql("findByUserId")
SELECT
  DISTINCT
  m.id,
  m.pid,
  m.name,
  m.code,
  m.icon,
  m.num,
  m.tips,
  m.ismenu,
  m.isopen,
  m.levels,
  m.pcode,
  m.pcodes,
  m.status,
  m.url
FROM jp_menu m, jp_role_menu rm, jp_user_role ur
WHERE m.id = rm.menuid
      AND rm.roleid = ur.roleid
      AND m.status = 1
      AND ur.userid = ?
    ORDER BY m.id,m.num
#end
